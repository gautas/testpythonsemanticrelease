.PHONY: run

run:
	docker login registry.gitlab.com/gautas/pydev -u local -p $(shell cat pat)
	docker run -it -d --rm --name pydev -v ~/dockvol:/dockvol -v ~/.gitconfig:/etc/gitconfig registry.gitlab.com/gautas/pydev:1.0.0
