# CHANGELOG



## v0.3.0 (2023-12-30)

### Feature

* feat: dummy [no ci] ([`096c01c`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/096c01cf5a1e72832484086ad60bfdb27f6fa806))


## v0.2.0 (2023-12-30)

### Ci

* ci: add build dependency to the release job ([`7ee5b3b`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/7ee5b3b1e7133212650ec339d605dc3539db9a13))

* ci: Merge branch &#39;1-ci-push-to-container-registry-in-release-step&#39; into &#39;main&#39;

Resolve &#34;ci: push to container registry in release step&#34;

Closes #1

See merge request gautas/testpythonsemanticrelease!1 ([`cd6ce41`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/cd6ce41d7fa034a717350820688eb3edbcc1533e))

* ci: add a publish job ([`e213640`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/e213640ad16283f0b1068317c47a7791179a0628))

### Feature

* feat: dummy [no ci] ([`bf81c31`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/bf81c312afe142a5df6f36e8ad1a15e1d40019e0))

### Unknown

* feat:dummy ([`803c17b`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/803c17bd646b633ae3d4189bbe92515d72bf64e3))


## v0.1.0 (2023-12-28)

### Chore

* chore: fix the make run command ([`4c96011`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/4c96011379b58e5b1e37cd9689e54c5e6d0f7c74))

### Ci

* ci: add gitlab token variable ([`f04bb9b`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/f04bb9bae6f1e65cc3b32afbff7b082f76806312))

* ci: add safe directory to release job ([`a210ecb`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/a210ecbd1fb18fe7f5b17e30c23544a0db605ac9))

* ci: git config in release step ([`ded7456`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/ded7456a0dd7bda2080cdf5840fc7b959d6e23a6))

* ci: checkout branch in release ([`5e740ad`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/5e740ad9cf127f0658a021c55e98e8dea84789fe))

* ci: clone instead of fetch in release ([`5d4853d`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/5d4853d7a5992a1b6853cc6e6ee2aaf660791ccb))

* ci: add build and release stages ([`584ec9d`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/584ec9dfabbc868fdb4875153670db0f3a4260fb))

* ci: add semantic-release config ([`93eebb3`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/93eebb30707f266ff89d9fb236aa8a3c08d669f6))

### Feature

* feat: start package ([`90dcbc0`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/90dcbc0c0a180b59a1c1168b8f09eb8167ac765b))

### Unknown

* Initial commit ([`2039da3`](https://gitlab.com/gautas/testpythonsemanticrelease/-/commit/2039da399037402ac8807c2b7500ec9f64c5cc73))
